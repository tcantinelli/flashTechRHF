module.exports = {
  tabWidth: 4,
  useTabs: false,
  semi: false,
  bracketSpacing: true,
  bracketSameLine: true,
  singleQuote: true,
  trailingComma: 'none',
  arrowParens: 'avoid',
  printWidth: 100
};

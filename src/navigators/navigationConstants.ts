enum navigationConstants {
  HOME = 'Home',
  FORM_ONE = 'FormOne',
  FORM_TWO = 'FormTwo',
  FORM_THREE = 'FormThree',
  VALIDATION = 'Validation'
}

export default navigationConstants;

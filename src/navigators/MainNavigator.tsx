import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import React from 'react'

import navigationConstants from './navigationConstants'
import { MainStackParamList } from './navigationModels'
import FormOneScreen from '@screens/FormOneScreen'
import FormTwoScreen from '@screens/FormTwoScreen'
import FormThreeScreen from '@screens/FormThreeScreen'

const Stack = createBottomTabNavigator<MainStackParamList>()

const MainNavigator = () => {
    return (
        <Stack.Navigator
            initialRouteName={navigationConstants.FORM_ONE}
            sceneContainerStyle={{
                backgroundColor: '#121212'
            }}
            screenOptions={{
                tabBarStyle: {
                    backgroundColor: '#121212',
                    borderTopColor: '#EC5990',
                    borderTopWidth: 2
                },
                headerShown: false,
                tabBarActiveTintColor: '#EC5990',
                tabBarLabelStyle: {
                    fontSize: 16,
                    top: 3,
                    left: 6
                },
                tabBarLabelPosition: 'beside-icon'
            }}>
            <Stack.Screen
                name={navigationConstants.FORM_ONE}
                component={FormOneScreen}
                options={{
                    title: 'FORM 1',
                    tabBarIconStyle: { display: 'none' }
                }}
            />
            <Stack.Screen
                name={navigationConstants.FORM_TWO}
                component={FormTwoScreen}
                options={{
                    title: 'FORM 2',
                    tabBarIconStyle: { display: 'none' }
                }}
            />
            <Stack.Screen
                name={navigationConstants.FORM_THREE}
                component={FormThreeScreen}
                options={{
                    title: 'FORM 3',
                    tabBarIconStyle: { display: 'none' }
                }}
            />
        </Stack.Navigator>
    )
}

export default MainNavigator

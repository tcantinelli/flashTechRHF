import React from 'react'

import navigationConstants from './navigationConstants'
import { RootStackParamList } from './navigationModels'
import MainNavigator from './MainNavigator'
import ValidationScreen from '@screens/ValidationScreen'
import { createStackNavigator } from '@react-navigation/stack'

const Stack = createStackNavigator<RootStackParamList>()

const RootNavigator = () => {
    return (
        <Stack.Navigator initialRouteName={navigationConstants.HOME}>
            <Stack.Screen
                name={navigationConstants.HOME}
                component={MainNavigator}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name={navigationConstants.VALIDATION}
                component={ValidationScreen}
                options={{
                    headerBackTitleVisible: false,
                    headerStyle: { backgroundColor: '#121212' },
                    headerTintColor: '#EC5990'
                }}
            />
        </Stack.Navigator>
    )
}

export default RootNavigator

import { MyForm } from '@screens/FormOneScreen'
import navigationConstants from './navigationConstants'
import { BottomTabNavigationProp } from '@react-navigation/bottom-tabs'
import { CompositeNavigationProp, NavigatorScreenParams } from '@react-navigation/native'
import { StackNavigationProp, StackScreenProps } from '@react-navigation/stack'
import { FormTwo } from '@screens/FormTwoScreen'
import { FormThree } from '@screens/FormThreeScreen'

export type RootStackParamList = {
    Home: NavigatorScreenParams<MainStackParamList>
    Validation: { formValues: MyForm | FormTwo | FormThree }
}

export type RootScreenNavigationProps<T extends keyof RootStackParamList> = CompositeNavigationProp<
    StackNavigationProp<RootStackParamList, T>,
    StackNavigationProp<RootStackParamList>
>

export type ValidationScreenProps = StackScreenProps<
    RootStackParamList,
    navigationConstants.VALIDATION
>

export type MainStackParamList = {
    FormOne: undefined
    FormTwo: undefined
    FormThree: undefined
}

export type MainScreenNavigationProps<T extends keyof MainStackParamList> = CompositeNavigationProp<
    BottomTabNavigationProp<MainStackParamList, T>,
    StackNavigationProp<RootStackParamList>
>

declare global {
    namespace ReactNavigation {
        interface RootParamList extends RootStackParamList {}
    }
}

export enum Icons {
    rhf = require('./icons/react-hook-form-logo-only.png'),
    react = require('./icons/react.png'),
    rocket = require('./icons/rocket-solid.png')
}

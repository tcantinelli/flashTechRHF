import { Icons } from '@assets/icons'
import React, { useEffect, useState } from 'react'
import { Controller, useFormContext } from 'react-hook-form'
import { Image, StyleSheet, Text, TextInput, View } from 'react-native'

interface CustomTextInputProps {
    label: string
    value: string
    setValue: (text: string) => void
    icon?: Icons
}

const CustomTextInput = ({ label, value, setValue, icon }: CustomTextInputProps): JSX.Element => {
    const [isFocused, setIsFocused] = useState(false)

    return (
        <View style={styles.container}>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 12 }}>
                {icon && <Image source={icon} style={styles.icon} resizeMode="contain" />}
                <Text style={styles.label}>{label}</Text>
            </View>
            <TextInput
                style={{
                    ...styles.input,
                    borderColor: isFocused ? '#EC5990' : 'white'
                }}
                onBlur={() => setIsFocused(false)}
                onFocus={() => setIsFocused(true)}
                onChangeText={(text: string) => setValue(text)}
                value={value}
                autoCorrect={false}
                clearButtonMode="while-editing"
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginBottom: 40,
        position: 'relative'
    },
    input: {
        alignSelf: 'stretch',
        borderWidth: 2,
        borderRadius: 8,
        borderStyle: 'solid',
        paddingBottom: 12,
        paddingHorizontal: 16,
        paddingTop: 16,
        color: '#F4F5F8',
        fontSize: 16
    },
    label: {
        backgroundColor: '#121212',
        color: '#F4F5F8',
        fontSize: 16,
        fontWeight: 'bold'
    },
    icon: {
        width: 22,
        height: 22,
        marginRight: 4
    }
})

export default CustomTextInput

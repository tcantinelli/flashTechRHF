import React, { useState } from 'react'
import { Controller } from 'react-hook-form'
import { StyleSheet, Text, View } from 'react-native'
import RadioButton from './RadioButton'
import { FlatList } from 'react-native-gesture-handler'

export enum MaritalStatusEnum {
    single = 'single',
    married = 'married',
    divorced = 'divorced',
    other = 'other'
}

export const maritalStatusLabel: Record<MaritalStatusEnum, string> = {
    single: 'Célibataire',
    married: 'Marié(e)',
    divorced: 'Divorcé(e)',
    other: "C'est compliqué..."
}

const MaritalStatus = (): JSX.Element => {
    const data = [
        { label: maritalStatusLabel.single, value: MaritalStatusEnum.single },
        { label: maritalStatusLabel.married, value: MaritalStatusEnum.married },
        { label: maritalStatusLabel.divorced, value: MaritalStatusEnum.divorced },
        { label: maritalStatusLabel.other, value: MaritalStatusEnum.other }
    ]

    return (
        <View>
            <Text style={styles.label}>Statut marital</Text>
            <Controller
                name="maritalStatus"
                render={({ field: { onChange, value } }) => (
                    <View style={styles.container}>
                        <FlatList
                            data={data}
                            renderItem={({ item, index }) => (
                                <RadioButton
                                    label={item.label}
                                    value={item.value}
                                    onPress={onChange}
                                    isSelected={value === item.value}
                                />
                            )}
                            keyExtractor={item => item.value}
                        />
                    </View>
                )}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginTop: 4,
        marginBottom: 20
    },
    label: {
        backgroundColor: '#121212',
        color: '#F4F5F8',
        fontSize: 16,
        fontWeight: 'bold'
    },
    errorMessage: {
        position: 'absolute',
        bottom: -22,
        right: 0,
        backgroundColor: '#121212',
        color: '#EE4444',
        textAlign: 'right',
        fontSize: 14,
        fontWeight: 'bold',
        marginRight: 4
    }
})

export default MaritalStatus

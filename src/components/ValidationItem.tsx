import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

interface ValidationItemProps {
    input: string
    value: string
}

const ValidationItem = ({ input, value }: ValidationItemProps): JSX.Element => {
    return (
        <View style={styles.container}>
            <Text style={styles.input}>{`${
                input.charAt(0).toUpperCase() + input.slice(1)
            }: `}</Text>
            <Text style={styles.value}>{value}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        marginBottom: 24
    },
    input: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#EC5990'
    },
    value: {
        fontSize: 18,
        marginLeft: 8,
        color: 'white'
    }
})

export default ValidationItem

import { Icons } from '@assets/icons'
import navigationConstants from '@navigators/navigationConstants'
import { navigate } from '@navigators/navigationServices'
import React from 'react'
import { FieldValues, useFormContext } from 'react-hook-form'
import { Image, Pressable, StyleSheet, Text } from 'react-native'

const SubmitButton = (): JSX.Element => {
    const { handleSubmit, reset } = useFormContext()

    const onSubmit = (values: FieldValues) => {
        navigate(navigationConstants.VALIDATION, {
            formValues: { ...values }
        })
        reset()
    }

    const onError = (values: FieldValues) => {
        console.log(values)
    }

    return (
        <Pressable style={styles.container} onPress={handleSubmit(onSubmit, onError)}>
            <Image source={Icons.rocket} style={styles.icon} />
            <Text style={styles.label}>Valider</Text>
        </Pressable>
    )
}

const styles = StyleSheet.create({
    container: {
        borderColor: '#069987',
        backgroundColor: '#069987',
        flexDirection: 'row',
        justifyContent: 'center',
        borderWidth: 2,
        borderRadius: 10,
        padding: 10,
        marginVertical: 20
    },
    icon: {
        tintColor: 'white',
        width: 25,
        height: 25,
        marginRight: 10
    },
    label: {
        color: 'white',
        fontSize: 16,
        alignSelf: 'center',
        fontWeight: 'bold'
    }
})

export default SubmitButton

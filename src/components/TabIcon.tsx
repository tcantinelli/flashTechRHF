import { Icons } from '@assets/icons'
import React from 'react'
import { Image, StyleSheet } from 'react-native'

interface TabIconProps {
    src: Icons
    focused: boolean
}

const TabIcon = ({ src, focused }: TabIconProps): JSX.Element => {
    return (
        <Image
            source={src}
            style={{ ...styles.icon, tintColor: focused ? undefined : 'gray' }}
            resizeMode="contain"
        />
    )
}

const styles = StyleSheet.create({
    icon: {
        width: 30,
        height: 30
    }
})

export default TabIcon

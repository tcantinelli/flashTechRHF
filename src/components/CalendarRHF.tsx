import React from 'react'
import { Controller } from 'react-hook-form'
import { StyleSheet, Text, View } from 'react-native'
import DatePicker from 'react-native-date-picker'

interface CalendarRHFProps {
    name: string
    label: string
    rules: object
}

const CalendarRHF = ({ name, label, rules }: CalendarRHFProps): JSX.Element => {
    return (
        <View style={styles.container}>
            <Text style={styles.label}>{label}</Text>
            <View style={styles.dateContainer}>
                <Controller
                    name={name}
                    rules={{ ...rules }}
                    render={({ field: { onChange, value }, fieldState: { error } }) => (
                        <>
                            <DatePicker
                                date={new Date(value)}
                                onDateChange={date => onChange(date.toISOString())}
                                textColor="#ffffff"
                                mode="date"
                            />

                            {error && <Text style={styles.errorMessage}>{error.message}</Text>}
                        </>
                    )}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingBottom: 30
    },
    dateContainer: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
    label: {
        backgroundColor: '#121212',
        color: '#F4F5F8',
        fontSize: 16,
        fontWeight: 'bold'
    },
    errorMessage: {
        position: 'absolute',
        bottom: -18,
        right: 18,
        backgroundColor: '#121212',
        color: '#EE4444',
        textAlign: 'right',
        fontSize: 14,
        fontWeight: 'bold',
        marginRight: 4
    }
})

export default CalendarRHF

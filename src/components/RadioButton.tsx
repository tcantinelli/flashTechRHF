import React from 'react'
import { Pressable, StyleSheet, Text, View } from 'react-native'

interface RadioButtonProps {
    label: string
    value: string
    isSelected: boolean
    onPress: (value: string) => void
    style?: object
}

const RadioButton = ({
    label,
    value,
    onPress,
    isSelected,
    style
}: RadioButtonProps): JSX.Element => {
    const handleOnPress = () => onPress(value)
    return (
        <Pressable onPress={handleOnPress} style={[styles.container, style]}>
            <View
                style={{
                    ...styles.radioContainer,
                    borderColor: 'white'
                }}>
                <View
                    style={{
                        ...styles.radioDot,
                        backgroundColor: isSelected ? '#EC5990' : 'transparent'
                    }}
                />
            </View>
            <Text style={{ ...styles.label, color: 'white' }}>{label}</Text>
        </Pressable>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        marginTop: 20
    },
    radioContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        padding: 4,
        borderRadius: 50,
        borderWidth: 2
    },
    radioDot: {
        flexDirection: 'row',
        width: 12,
        height: 12,
        borderRadius: 50
    },
    label: {
        fontSize: 16,
        flex: 1,
        marginLeft: 12,
        fontWeight: '500',
        alignSelf: 'center'
    }
})

export default RadioButton

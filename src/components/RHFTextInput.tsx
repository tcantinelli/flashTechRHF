import { Icons } from '@assets/icons'
import React, { useState } from 'react'
import { Controller } from 'react-hook-form'
import { Image, StyleSheet, Text, TextInput, View } from 'react-native'

interface RHFTextInputProps {
    label: string
    name: string
    rules?: object
    secureTextEntry?: boolean
    autoCapitalize?: 'characters' | 'words' | 'sentences' | 'none'
    editable?: boolean
    icon?: Icons
}

const RHFTextInput = ({
    label,
    name,
    rules = {},
    secureTextEntry = false,
    autoCapitalize = 'sentences',
    editable = true,
    icon
}: RHFTextInputProps): JSX.Element => {
    const [isFocused, setIsFocused] = useState(false)

    return (
        <View style={styles.container}>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 12 }}>
                {icon && <Image source={icon} style={styles.icon} resizeMode="contain" />}
                <Text style={styles.label}>{label}</Text>
            </View>
            <Controller
                name={name}
                rules={{ ...rules }}
                render={({ field: { onChange, onBlur, value }, fieldState: { error } }) => (
                    <>
                        <TextInput
                            style={{
                                ...styles.input,
                                borderColor: isFocused ? '#EC5990' : 'white',
                                backgroundColor: error
                                    ? '#FF6E6E'
                                    : editable
                                    ? 'transparent'
                                    : '#524F4F'
                            }}
                            onBlur={() => {
                                onBlur()
                                setIsFocused(false)
                            }}
                            onFocus={() => setIsFocused(true)}
                            onChangeText={onChange}
                            value={value}
                            autoCorrect={false}
                            clearButtonMode="while-editing"
                            secureTextEntry={secureTextEntry}
                            autoCapitalize={autoCapitalize}
                            editable={editable}
                        />

                        {error && <Text style={styles.errorMessage}>{error.message}</Text>}
                    </>
                )}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginBottom: 40,
        position: 'relative'
    },
    input: {
        alignSelf: 'stretch',
        borderWidth: 2,
        borderRadius: 8,
        borderStyle: 'solid',
        paddingBottom: 12,
        paddingHorizontal: 16,
        paddingTop: 16,
        color: '#F4F5F8',
        fontSize: 16
    },
    label: {
        backgroundColor: '#121212',
        color: '#F4F5F8',
        fontSize: 16,
        fontWeight: 'bold',
        alignSelf: 'center',
    },
    errorMessage: {
        position: 'absolute',
        bottom: -22,
        right: 0,
        backgroundColor: '#121212',
        color: '#EE4444',
        textAlign: 'right',
        fontSize: 14,
        fontWeight: 'bold',
        marginRight: 4
    },
    icon: {
        width: 20,
        height: 20,
        marginRight: 8
    }
})

export default RHFTextInput

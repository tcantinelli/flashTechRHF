import ValidationItem from '@components/ValidationItem'
import { ValidationScreenProps } from '@navigators/navigationModels'
import React from 'react'
import { FlatList, StyleSheet, View } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import Rive from 'rive-react-native'

const ValidationScreen = ({ route }: ValidationScreenProps): JSX.Element => {
    const { formValues } = route.params

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#121212' }}>
            <Rive resourceName="stick_man" />
            <View style={styles.container}>
                <FlatList
                    data={Object.entries(formValues)}
                    renderItem={({ item }) => (
                        <ValidationItem input={item[0]} value={item[1] as string} />
                    )}
                    keyExtractor={(item, index) => `${item}-${index}`}
                    style={{ marginTop: 10 }}
                />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#121212',
        flex: 1,
        paddingHorizontal: 16
    }
})

export default ValidationScreen

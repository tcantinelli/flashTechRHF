import { Icons } from '@assets/icons'
import CustomTextInput from '@components/CustomTextInput'
import RHFTextInput from '@components/RHFTextInput'
import SubmitButton from '@components/SubmitButton'
import { required } from '@constants/RhfRules'
import React, { useEffect, useState } from 'react'

import { FieldValues, FormProvider, useForm } from 'react-hook-form'

import { StyleSheet, Text, View } from 'react-native'

export interface FormThree extends FieldValues {
    reactNative: string
    rhf: string
}

const defaultValues: FormThree = {
    reactNative: '',
    rhf: ''
}

let render = 0

const FormThreeScreen = () => {
    const methods = useForm<FormThree>({
        defaultValues: defaultValues
    })

    const [data, setData] = useState('')

    useEffect(() => {
        if (data) methods.setValue('reactNative', data)
    }, [data])

    render++

    return (
        <FormProvider {...methods}>
            <View style={styles.container}>
                <View style={styles.renderContainer}>
                    <Text style={styles.render}>Render:  {render}</Text>
                </View>
                <CustomTextInput label="React Native" value={data} setValue={setData} icon={Icons.react}/>
                <RHFTextInput label="React Hook Form" name="rhf" rules={required} icon={Icons.rhf} />
                <SubmitButton />
            </View>
        </FormProvider>
    )
}

export default FormThreeScreen

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    container: {
        paddingHorizontal: 8,
        flex: 1,
        justifyContent: 'center'
    },
    renderContainer: {
        marginBottom: 60,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 16
    },
    render: {
        color: 'cyan',
        fontWeight: 'bold',
        fontSize: 32
    }
})

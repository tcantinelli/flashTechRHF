import RHFTextInput from '@components/RHFTextInput'
import SubmitButton from '@components/SubmitButton'
import { required } from '@constants/RhfRules'
import React from 'react'
import { FieldValues, FormProvider, useForm } from 'react-hook-form'

import { StyleSheet, View } from 'react-native'
import CalendarRHF from '@components/CalendarRHF'

import differenceInCalendarYears from 'date-fns/differenceInCalendarYears'
import MaritalStatus, { MaritalStatusEnum } from '@components/MaritalStatus'
import { useSafeAreaInsets } from 'react-native-safe-area-context'

export interface FormTwo extends FieldValues {
    maritalStatus: MaritalStatusEnum
    birthday: string
}

const defaultValues: FormTwo = {
    maritalStatus: MaritalStatusEnum.single,
    birthday: new Date().toISOString()
}

const checkBirthDay = (value: string): boolean | string => {
    const today = new Date()
    const currentBirthday = new Date(value)
    return differenceInCalendarYears(today, currentBirthday) >= 18 || 'Trop jeune !!'
}

const birthdayRules = {
    validate: (value: string) => checkBirthDay(value)
}

const FormTwoScreen = () => {
    const insets = useSafeAreaInsets()

    const methods = useForm<FormTwo>({
        defaultValues: defaultValues
    })

    return (
        <FormProvider {...methods}>
            <View style={{ ...styles.mainContainer, paddingTop: insets.top + 16 }}>
                <CalendarRHF label="Date de naissance" name="birthday" rules={birthdayRules} />
                <MaritalStatus />
                <SubmitButton />
            </View>
        </FormProvider>
    )
}

export default FormTwoScreen

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        paddingHorizontal: 8,
        paddingBottom: 40
    },
    container: {
        justifyContent: 'center'
    }
})

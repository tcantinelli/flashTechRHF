import RHFTextInput from '@components/RHFTextInput'
import SubmitButton from '@components/SubmitButton'
import { required } from '@constants/RhfRules'
import React, { useEffect } from 'react'

import { FieldValues, FormProvider, useForm } from 'react-hook-form'

import { StyleSheet, Text, View } from 'react-native'

export interface MyForm extends FieldValues {
    firstName: string
    lastName: string
    email: string
    pseudo: string
}

const defaultValues: MyForm = {
    firstName: '',
    lastName: '',
    email: '',
    pseudo: ''
}

export const firstNameRules = {
    required: 'Ce champ est requis',
    minLength: {
        value: 2,
        message: 'Deux caractères minimum'
    }
}

export const emailRules = {
    required: 'Ce champ est requis',
    pattern: {
        value: /^[a-zA-ZÀ-ÿ0-9._-]+@[a-zA-ZÀ-ÿ0-9.-]+\.[a-zA-ZÀ-ÿ0-9-]{2,}$/,
        message: 'Format incorrect'
    },
    minLength: {
        value: 3,
        message: 'Entre 3 et 100 caractères'
    },
    maxLength: {
        value: 100,
        message: 'Entre 3 et 100 caractères'
    }
}

const FormOneScreen = () => {
    const methods = useForm<MyForm>({
        defaultValues: defaultValues
    })

    const firstName = methods.watch('firstName')
    const lastName = methods.watch('lastName')

    useEffect(() => {
        if (firstName && lastName) {
            methods.setValue('pseudo', `${firstName} ${lastName[0].toUpperCase()}.`)
        }
    }, [lastName, firstName])

    return (
        <FormProvider {...methods}>
            <View style={styles.container}>
                <RHFTextInput label="Nom" name="lastName" rules={required} />
                <RHFTextInput label="Prénom" name="firstName" rules={firstNameRules} />
                <RHFTextInput label="Pseudo" name="pseudo" editable={false} />
                <RHFTextInput label="Email" name="email" autoCapitalize="none" rules={emailRules} />
                <SubmitButton />
            </View>
        </FormProvider>
    )
}

export default FormOneScreen

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    container: {
        paddingHorizontal: 8,
        flex: 1,
        justifyContent: 'center'
    },
    render: {
        color: 'white'
    }
})

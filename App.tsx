/**
 * boilerplate App
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import MainNavigator from '@navigators/MainNavigator'
import RootNavigator from '@navigators/RootNavigator'
import { getCurrentRoute, navigationRef } from '@navigators/navigationServices'
import { NavigationContainer } from '@react-navigation/native'
import { initApp, setCurrentRoute } from '@redux/reducers/general'
import store, { persistor } from '@redux/store'

import React, { useEffect } from 'react'
import { SafeAreaProvider } from 'react-native-safe-area-context'
import { Provider } from 'react-redux'

import { PersistGate } from 'redux-persist/integration/react'

// import crashlyticsUtils from '@utils/crashlyticsUtils';

// crashlyticsUtils();

const App = () => {
    useEffect(() => {
        store.dispatch(initApp())
    }, [])

    return (
        <Provider store={store}>
            <PersistGate persistor={persistor}>
                <SafeAreaProvider>
                    <NavigationContainer
                        ref={navigationRef}
                        onStateChange={() => {
                            const currentRouteName = getCurrentRoute()?.name
                            store.dispatch(setCurrentRoute(currentRouteName))
                        }}>
                        <RootNavigator />
                    </NavigationContainer>
                </SafeAreaProvider>
            </PersistGate>
        </Provider>
    )
}

export default App
